from django.apps import AppConfig


class ViewCoursesConfig(AppConfig):
    name = 'viewcourses'
