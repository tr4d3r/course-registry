from django.urls import path

from . import views

urlpatterns = [
    path('course/<int:id>', views.course, name='course'),
]