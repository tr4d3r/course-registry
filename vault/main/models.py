from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator


# Create your models here.
class ToDoList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="todolist", null=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Item(models.Model):
    todolist = models.ForeignKey(ToDoList, on_delete=models.CASCADE)
    text = models.CharField(max_length=300)
    complete = models.BooleanField()
    
    def __str__(self):
        return self.text

class Course(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField(max_length=5000, blank=True,
                                validators=[MaxLengthValidator(5000)])
    category = models.CharField(max_length=300)
    training_team = models.CharField(max_length=300)
    starts = models.DateTimeField()
    ends = models.DateTimeField()
    registration_open = models.BooleanField(default=True)
    unlimited_seats = models.BooleanField(default=False)
    total_seats = models.IntegerField()
    virtual = models.BooleanField(default=False)
    location = models.CharField(max_length=400, default="TBD")

class CourseRoster(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="courseroster", null=True)
    users = models.ForeignKey(User, on_delete=models.CASCADE, related_name="courseroster", null=True)




    



