# contains all paths to our pages 
from django.urls import path

from . import views


# define root landing for webapp 
urlpatterns = [
    path('<int:id>', views.index, name = 'index'),
    path('', views.home, name = 'home'),
    path('create/', views.create, name = 'create'),
    path('home/', views.home, name='home'),
    path('view/', views.view, name = 'view'),
    path('create_course/', views.create_course, name='create course')
]
