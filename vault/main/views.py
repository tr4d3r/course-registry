from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ToDoList, Item, CourseRoster
from .forms import CreateNewList, NewCourseForm 
from django.http import HttpResponseRedirect
# Create your views here.

# inside views.py
def index(response, id):
    ls = ToDoList.objects.get(id=id)

    if ls in response.user.todolist.all():

        if response.method == "POST":
            if response.POST.get("save"):
                for item in ls.item_set.all():
                    if response.POST.get("c" + str(item.id)) == "clicked":
                        item.complete = True
                    else:
                        item.complete = False

                    item.save()

            elif response.POST.get("newItem"):
                txt = response.POST.get("new")

                if len(txt) > 2:
                    ls.item_set.create(text=txt, complete=False)
                else:
                    print("invalid")

        return render(response, "main/list.html", {"ls":ls})

    return render(response, "main/home.html", {})

def home(response):
    return render(response, 'main/home.html', {})

def create(response):
    #will give you the user so we can run .is_authenticated for example
    #response.user
    if response.method == 'POST':
        form = CreateNewList(response.POST)
        print('post received')
        if form.is_valid():
            print('form is valid')
            n = form.cleaned_data['name']
            t = ToDoList(name=n)
            t.save()
            response.user.todolist.add(t)

            return HttpResponseRedirect('/%i' %t.id)

    else:
        print('else triggered')
        form = CreateNewList()

    print('returned')
    return render(response, 'main/create.html', {'form': form})

def view(response):
    return render(response, "main/view.html", {})

def create_course(response):
    
    if response.method == 'POST':
        form = NewCourseForm(response.POST)
        roster = CourseRoster()
        n = form['title']
        t = ToDoList(id='tile')
        if form.is_valid():
            form.save()
            roster.save(t)
        return redirect('/home')
    else:
        form = NewCourseForm()
    return render(response, "main/coursecreate.html", {"form": form})