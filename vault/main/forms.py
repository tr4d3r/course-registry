from django import forms
from .models import Course

class CreateNewList(forms.Form):
    name = forms.CharField(label='name', max_length=200)
    
class NewCourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['title', 'description', 'category',
        'training_team', 'starts', 'ends', 'registration_open', 
        'unlimited_seats', 'total_seats', 'location', 'virtual']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'category': forms.TextInput(attrs={'class': 'form-control'}),
            'training_team': forms.TextInput(attrs={'class': 'form-control'}),
            'starts': forms.TextInput(attrs={'class': 'form-control'}),
            'ends': forms.TextInput(attrs={'class': 'form-control'}),
            'total_seats': forms.TextInput(attrs={'class': 'form-control'}),
            'location': forms.TextInput(attrs={'class': 'form-control'}),
            }
        


