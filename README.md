# Vault

A simple course listing and registry service running on Django with PostgreSQL database.  

## Initial Setup 

Requirements 
* Docker 
* Docker-Compose
* Django


## Setting up the Database

* Deploy PostgreSQL + PG Admin to Docker 

```bash
docker-compose up -d
```

* Open PG Admin @localhost:5050
* Login using default credentials -u pgadmin4@pgadmin.org -p admin
* create new server using the following 
    * name: course-registry
    * host: postgres
    * user: demo
    * pass: demo

## Django Migrations
**via Taskfile**

```bash
run stage_migrations
run make_migrations
```

## Starting the local dev server 

**via Taskfile**

```bash
run server
```
**stop server**
ctrl+c or... 
```bash
run stop
```

## Create Django Admin Super User 

```bash
run super_user
```

## Manually view a course

to manually view a course hit this endpoint 
```
/course/{id}
```
where id = the index of the course you are accessing.  

for a course at index 5 use:
```
/course/5
```